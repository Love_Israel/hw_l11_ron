#include "threads.h"
#include <thread>
#include <chrono>

using namespace std::chrono;

void I_Love_Threads()
{
	std::cout << "I Love Threads ! \n";
}


void call_I_Love_Threads()
{
	std::thread thr(I_Love_Threads);
	thr.join();
}

//printing vector
void printVector(std::vector<int> primes) 
{
	for (auto i : primes) 
	{
		std::cout << i << ", ";
	}
}

//calc prime numbers in given range
void getPrimes(int begin, int end, std::vector<int>& primes) 
{
	milliseconds ms = duration_cast<milliseconds>(
	system_clock::now().time_since_epoch()
		);

	printf("curr time is %d", ms);
	for (int i = begin ; i <= end ; i++) 
	{
		for (int j = 2 ; j < i ; j++) 
		{
			if (i % j == 0)
				goto no_prime;
		}
	primes.push_back(i);
	no_prime: {};
	}
	ms = duration_cast<milliseconds>(
		system_clock::now().time_since_epoch()
		);
	printf("curr time is %d\n", ms);
}

//create thread calculate the prime numbers in given range
std::vector<int> callGetPrimes(int begin, int end) 
{
	std::vector <int>* toRet = new std::vector<int>();
	std::thread thr(getPrimes, begin, end, std::ref(*toRet));
	thr.join();
	return *toRet;
}

//the instructions told us not to use getPrimes()
void writePrimesToFile(int begin, int end, std::ofstream& file) 
{
	for (int i = begin; i <= end; i++)
	{
		for (int j = 2; j < i; j++)
		{
			if (i % j == 0)
				goto no_prime;
		}
		file << i << ", ";
	no_prime: {};
	}

}

//using multipy threads to write prime numbers in range to file
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	milliseconds ms = duration_cast<milliseconds>(
		system_clock::now().time_since_epoch()
		);
	printf("curr time is %d\n", ms);
	std::thread* thr;
	std::ofstream file;
	file.open(filePath);
	int counter = 0;
	int beg = begin;
	for (int i = 0 ; i < N ; i++) 
	{
		thr = new std::thread(writePrimesToFile, begin, ((end - beg)/N) * (i + 1), std::ref(file));
		begin = ((end - begin) / N) * (i + 1);
		thr->join();
	}
	ms = duration_cast<milliseconds>(
		system_clock::now().time_since_epoch()
		);
	printf("curr time is %d\n", ms);
}